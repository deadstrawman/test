alias \
    a="sudo su"\
    c="sudo pacman -Rns $(pacman -Qtdq)"\
    d="sudo pacman -R"\
    g="/usr/bin/g &"\
    i="sudo pacman -S"\
    l="~/s/l &"\
    m="~/s/a/m &"\
    n="~/s/.n"\
    o="sudo shutdown -P now"\
    p="pacman"\
    q="pacman -Q | wc -l"\
    s="startx"\
    u="~/s/ufetch/ufetch-artix"\
    v="nvim"\
    w="~/docs/kjv/kjv"\
    z="zathura"
PROMPT='%F{044}%~%f -> '


