set bg=dark
set nu rnu
syntax on
set encoding=utf-8
set title
set wildmode=longest,list,full
set nobackup
set nowritebackup
set noswapfile
set dir=/tmp
set path+=**
set shiftwidth=4
set expandtab
set tabstop=4
set softtabstop=4

